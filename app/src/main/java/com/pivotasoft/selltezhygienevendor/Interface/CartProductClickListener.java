package com.pivotasoft.selltezhygienevendor.Interface;

import com.pivotasoft.selltezhygienevendor.Model.Product;

public interface CartProductClickListener {

    void onMinusClick(Product product);

    void onPlusClick(Product product);

    void onSaveClick(Product product);

    void onRemoveDialog(Product product);

    void onWishListDialog(Product product);

}
