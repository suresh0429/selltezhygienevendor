package com.pivotasoft.selltezhygienevendor.Interface;

import com.pivotasoft.selltezhygienevendor.Response.CheckoutResponse;

import java.util.List;

public interface PaymentTypeInterface {

    void onItemClick(List<CheckoutResponse.DataBean.PaymentGatewayBean> paymentGatewayBean, int position);
}
