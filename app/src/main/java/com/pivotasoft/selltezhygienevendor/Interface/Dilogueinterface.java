package com.pivotasoft.selltezhygienevendor.Interface;

import com.pivotasoft.selltezhygienevendor.Model.OrderIdItem;

public interface Dilogueinterface {
    void onInvoiceClick(OrderIdItem orderListBean);
    void onUpdateStatusClick(OrderIdItem orderListBean);
}
